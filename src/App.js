import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Email from "./components/Email";

function App() {
  return (
    <div className="App">
      <Email/>
    </div>
  );
}

export default App;

