import React, {  Component } from 'react';
import {Container, Form, Button, Row, Col, Alert} from "react-bootstrap";
import axios from 'axios';
import LoadingBar from 'react-top-loading-bar';

class Email extends Component {
    state = {
        emailError : '',
        emailValid: false,
        email : '',
        emailApiRes: [],
        userObj: {},
        loadingBarProgress: 0
    };

    // Check Email Valid Format
    handleEmail = (event) => {
        const  emailId = event.target.value;
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (reg.test(emailId) === false) {
            this.setState({
                emailError : 'Please Enter Valid Email',
                userObj: {},
                emailValid: false
            });
            return false;
        }
        else {
            this.setState({email: emailId, emailError: '', emailValid: true});
        }
    };

    //call API
    onSubmit = () => {
        const { emailValid } = this.state;
        if(emailValid) {
            this.add(30);
            axios.get('https://reqres.in/api/users?page=1')
                .then(res => {
                    this.complete();
                    this.setState({emailApiRes: res.data.data});
                    this.emailValidate();
                }).catch(err => {
                this.complete();
                console.log(err);
            })
        }
    };
    // Email Id Validate from API
    emailValidate = () => {
        const { emailApiRes, email } = this.state;
       const emailExist = emailApiRes.filter(user =>
           user.email == email
       );
       if(emailExist.length > 0) {
    this.setState({
        userObj: emailExist[0]
    })
       } else {
        this.setState({
            emailError: 'Your Email Id does not Exist'
        })
       }
    };

// Loading stuff
    add = value => {
        this.setState({
            loadingBarProgress: this.state.loadingBarProgress + value
        });
    };

    complete = () => {
        this.setState({ loadingBarProgress: 100 });
    };

    onLoaderFinished = () => {
        this.setState({ loadingBarProgress: 0 });
    };

    render() {
        const {emailError, email, userObj} = this.state;
        return (
            <React.Fragment>
                <LoadingBar
                    progress={this.state.loadingBarProgress}
                    height={3}
                    color='red'
                    onLoaderFinished={() => this.onLoaderFinished()}
                />
            <div>
                <Container className="main">
                    <Row>
                        <Col md={{ span: 6, offset: 3 }}>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" required onChange={($event) => this.handleEmail($event)} placeholder="Enter email" />
                        { emailError !== '' ?
                            <Form.Text className="text-danger">
                                {emailError}
                            </Form.Text>
                            : null
                        }
                    </Form.Group>

                    <Button variant="primary" onClick={() => this.onSubmit()} type="submit">
                        Submit
                    </Button>
                            {Object.keys(userObj).length > 0 ?
                           <div className="user-info text-left">
                               <Alert  variant="success">
                                   <Alert.Heading>User Validated Successfully</Alert.Heading> <br />
                                   <h6><b>First Name </b> : {userObj.first_name}</h6>
                                   <h6><b>Last Name </b> : {userObj.last_name}</h6>
                               </Alert>
                           </div>
                                : null }
                        </Col>
                        </Row>
                </Container>
            </div>
                </React.Fragment>
        );
    }

}
export default Email
